import * as mongoose from 'mongoose';

export const RaceSchema = new mongoose.Schema({
    titleRace: { type: String, required: true },
    idStage: { type: mongoose.Schema.Types.ObjectId, required: true },
});
