import * as mongoose from 'mongoose';

export const TeamSchema = new mongoose.Schema({
    titleTeam: { type: String, required: true },
    members: { type: Number, required: true },
    idUser: { type: mongoose.Schema.Types.ObjectId, required: true },
    teamLogo: { type: String},
});
