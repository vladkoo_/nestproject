import * as mongoose from 'mongoose';

export const StageSchema = new mongoose.Schema({
    titleStage: { type: String, required: true },
    idLeague: { type: mongoose.Schema.Types.ObjectId, required: true },
});
