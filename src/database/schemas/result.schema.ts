import * as mongoose from 'mongoose';

export const ResultSchema = new mongoose.Schema({
    titleResult: { type: String, required: true },
    score: { type: Number, required: true },
    idRace: { type: mongoose.Schema.Types.ObjectId, required: true },
    idTeam: { type: mongoose.Schema.Types.ObjectId, required: true },
});
