import * as mongoose from 'mongoose';

export const SeasonSchema = new mongoose.Schema({
  titleSeason: { type: String, required: true },
});
