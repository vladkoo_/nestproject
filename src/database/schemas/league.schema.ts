import * as mongoose from 'mongoose';

export const LeagueSchema = new mongoose.Schema({
    titleLeague: { type: String, required: true },
    idSeason: { type: mongoose.Schema.Types.ObjectId, required: true },
});
