import * as mongoose from 'mongoose';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: (): Promise<typeof mongoose> => {
      mongoose.set('debug', true);
      mongoose.connection
        .on('close', () => console.log('Database connection is closed.'))
        .on('open', () => console.log('Mongodb successful connected. Listening on 27017 port.'));
      return mongoose.connect('mongodb://localhost:27017/serverexpress', { useNewUrlParser: true });
    },
  },
];
