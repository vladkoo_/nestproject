import { ApiModelProperty } from '@nestjs/swagger';

export class DeleteMongooseDto {
  @ApiModelProperty({
    example: '1',
    type: 'number',
  })
  readonly n: number;

  @ApiModelProperty({
    example: '1',
    type: 'number',
  })
  readonly ok: number;

  @ApiModelProperty({
    example: '1',
    type: 'number',
  })
  readonly deletedCount: number;
}
