import { ApiModelProperty } from '@nestjs/swagger';

export class EditMongooseDto {
  @ApiModelProperty({
    example: '1',
    type: 'number',
  })
  readonly n: number;

  @ApiModelProperty({
    example: '1',
    type: 'number',
  })
  readonly nModified: number;

  @ApiModelProperty({
    example: '1',
    type: 'number',
  })
  readonly ok: number;
}
