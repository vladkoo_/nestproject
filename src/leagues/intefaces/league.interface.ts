import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface League extends Document {
  readonly _id: mongoose.Schema.Types.ObjectId;
  readonly titleLeague: string;
}
