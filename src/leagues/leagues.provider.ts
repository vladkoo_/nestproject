import { Connection } from 'mongoose';
import { LeagueSchema } from '../database/schemas/league.schema';

export const LeaguesProviders = [
    {
        provide: 'LEAGUE_MODEL',
        useFactory: (connection: Connection) => connection.model('League', LeagueSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
