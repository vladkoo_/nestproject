import { Inject, Injectable } from '@nestjs/common';

import { Model } from 'mongoose';
import * as mongoose from 'mongoose';

import { League } from './intefaces/league.interface';

import { CreateLeagueDto } from './dto/create-league.dto';
import { EditLeagueDto } from './dto/edit-league.dto';

@Injectable()
export class LeaguesService {
  constructor(
    @Inject('LEAGUE_MODEL')
    private readonly leagueModel: Model<League>,
  ) {}

  async create(createLeagueDto: CreateLeagueDto): Promise<League> {
    return this.leagueModel.create(createLeagueDto);
  }

  async findAll(): Promise<League[]> {
    return await this.leagueModel.find().exec();
  }

  async editOne(editLeagueDto: EditLeagueDto): Promise<League> {
    return await this.leagueModel.updateOne({_id: editLeagueDto._id}, editLeagueDto);
  }

  async deleteOne(id: string): Promise<mongoose.mquery> {
    return await this.leagueModel.deleteOne({_id: id});
  }
}
