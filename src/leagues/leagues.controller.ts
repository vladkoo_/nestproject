import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';

import { CreateLeagueDto } from './dto/create-league.dto';
import { EditLeagueDto } from './dto/edit-league.dto';

import { League } from './intefaces/league.interface';

import { LeaguesService } from './leagues.service';
import { ApiOperation, ApiResponse, ApiInternalServerErrorResponse, ApiBadRequestResponse, ApiUseTags } from '@nestjs/swagger';
import { DeleteMongooseDto } from '../database/dto/delete-mongoose.dto';
import { EditMongooseDto } from '../database/dto/edit-mongoose.dto';

@ApiUseTags('leagues')
@Controller('leagues')
export class LeaguesController {
  constructor(private readonly leaguesService: LeaguesService) {}

  @ApiOperation({ title: 'Get all leagues' })
  @ApiResponse({
    status: 200,
    isArray: true,
    type: EditLeagueDto,
    description: 'OK',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @Get()
  async findAll(): Promise<League[]> {
    return this.leaguesService.findAll();
  }

  @ApiOperation({ title: 'Create new league' })
  @ApiResponse({
    status: 200,
    isArray: false,
    type: EditLeagueDto,
    description: 'OK',
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @Post()
  async create(@Body() createLeagueDto: CreateLeagueDto) {
    return this.leaguesService.create(createLeagueDto);
  }

  @ApiOperation({ title: 'Edit league' })
  @ApiResponse({
    status: 200,
    isArray: false,
    type: EditMongooseDto,
    description: 'OK',
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @Put()
  async editOne(@Body() editLeagueDto: EditLeagueDto) {
    return this.leaguesService.editOne(editLeagueDto);
  }

  @ApiOperation({ title: 'Delete league' })
  @ApiResponse({
    status: 200,
    isArray: false,
    type: DeleteMongooseDto,
    description: 'OK',
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    return this.leaguesService.deleteOne(id);
  }
}
