import { Module } from '@nestjs/common';

import { DatabaseModule } from '../database/database.module';
import { LeaguesProviders } from './leagues.provider';
import { LeaguesService } from './leagues.service';
import { LeaguesController } from './leagues.controller';

@Module(
    {
      imports: [DatabaseModule],
      controllers: [LeaguesController],
      providers: [
        LeaguesService,
        ...LeaguesProviders,
      ],
    },
)
export class LeaguesModule {}
