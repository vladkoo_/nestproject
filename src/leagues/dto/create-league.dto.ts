import { ApiModelProperty } from '@nestjs/swagger';
import * as mongoose from 'mongoose';

export class CreateLeagueDto {
  @ApiModelProperty({
    example: 'Easy League',
    type: 'string',
  })
  readonly titleLeague: string;

  @ApiModelProperty({
    example: '5ca4d62f2d36ae1c68f128ac',
    type: 'string',
  })
  readonly idSeason: mongoose.Schema.Types.ObjectId;
}
