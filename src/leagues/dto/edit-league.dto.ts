import { ApiModelProperty } from '@nestjs/swagger';
import * as mongoose from 'mongoose';

export class EditLeagueDto {
    @ApiModelProperty({
        example: '5ca4d7214c86ff1fec4e72ba',
        type: 'string',
    })
    readonly _id: mongoose.Schema.Types.ObjectId;

    @ApiModelProperty({
        example: 'Easy League',
        type: 'string',
    })
    readonly titleLeague: string;

    @ApiModelProperty({
        example: '5ca4d62f2d36ae1c68f128ac',
        type: 'string',
    })
    readonly idSeason: mongoose.Schema.Types.ObjectId;
}
