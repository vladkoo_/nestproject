import { Module, MiddlewareConsumer } from '@nestjs/common';
import { SeasonsModule } from './seasons/seasons.module';
import { LeaguesModule } from './leagues/leagues.module';
import { StagesModule } from './stages/stages.module';
import { RacesModule } from './races/races.module';
import { TeamsModule } from './teams/teams.module';
import { UsersModule } from './users/users.module';
import { ResultsModule } from './results/results.module';

@Module({
  imports: [SeasonsModule, LeaguesModule, StagesModule, RacesModule, TeamsModule, UsersModule, ResultsModule],
})
export class AppModule {}
