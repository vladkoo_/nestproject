import { Inject, Injectable } from '@nestjs/common';

import { Model } from 'mongoose';
import * as mongoose from 'mongoose';

import { Race } from './intefaces/race.interface';

import { CreateRaceDto } from './dto/create-race.dto';
import { EditRaceDto } from './dto/edit-race.dto';

@Injectable()
export class RacesService {
  constructor(
    @Inject('RACE_MODEL')
    private readonly raceModel: Model<Race>,
  ) {}

  async create(createRaceDto: CreateRaceDto): Promise<Race> {
    return this.raceModel.create(createRaceDto);
  }

  async findAll(): Promise<Race[]> {
    return await this.raceModel.find().exec();
  }

  async editOne(editRaceDto: EditRaceDto): Promise<Race> {
    return await this.raceModel.updateOne({_id: editRaceDto._id}, editRaceDto);
  }

  async deleteOne(id: string): Promise<mongoose.mquery> {
    return await this.raceModel.deleteOne({_id: id});
  }
}
