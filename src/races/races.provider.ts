import { Connection } from 'mongoose';
import { RaceSchema } from '../database/schemas/race.schema';

export const RacesProviders = [
    {
        provide: 'RACE_MODEL',
        useFactory: (connection: Connection) => connection.model('Race', RaceSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
