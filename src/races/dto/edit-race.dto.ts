import * as mongoose from 'mongoose';

export class EditRaceDto {
    readonly _id: mongoose.Schema.Types.ObjectId;
    readonly titleRace: string;
}
