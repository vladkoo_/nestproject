import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';

import { CreateRaceDto } from './dto/create-race.dto';
import { EditRaceDto } from './dto/edit-race.dto';

import { Race } from './intefaces/race.interface';

import { RacesService } from './races.service';

@Controller('races')
export class RacesController {
  constructor(private readonly racesService: RacesService) {}

  @Get()
  async findAll(): Promise<Race[]> {
    return this.racesService.findAll();
  }

  @Post()
  async create(@Body() createRaceDto: CreateRaceDto) {
    return this.racesService.create(createRaceDto);
  }

  @Put()
  async editOne(@Body() editRaceDto: EditRaceDto) {
    return this.racesService.editOne(editRaceDto);
  }

  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    return this.racesService.deleteOne(id);
  }
}
