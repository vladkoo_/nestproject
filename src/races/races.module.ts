import { Module } from '@nestjs/common';

import { DatabaseModule } from '../database/database.module';
import { RacesProviders } from './races.provider';
import { RacesService } from './races.service';
import { RacesController } from './races.controller';

@Module(
    {
      imports: [DatabaseModule],
      controllers: [RacesController],
      providers: [
        RacesService,
        ...RacesProviders,
      ],
    },
)
export class RacesModule {}
