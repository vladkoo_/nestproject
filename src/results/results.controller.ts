import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';

import { CreateResultDto } from './dto/create-result.dto';
import { EditResultDto } from './dto/edit-result.dto';

import { Result } from './intefaces/result.interface';

import { ResultsService } from './results.service';

@Controller('results')
export class ResultsController {
  constructor(private readonly resultsService: ResultsService) {}

  @Get()
  async findAll(): Promise<Result[]> {
    return this.resultsService.findAll();
  }

  @Post()
  async create(@Body() createResultDto: CreateResultDto) {
    return this.resultsService.create(createResultDto);
  }

  @Put()
  async editOne(@Body() editResultDto: EditResultDto) {
    return this.resultsService.editOne(editResultDto);
  }

  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    return this.resultsService.deleteOne(id);
  }
}
