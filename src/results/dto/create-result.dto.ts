import * as mongoose from 'mongoose';

export class CreateResultDto {
  readonly titleResult: string;
  readonly score: number;
  readonly idRace: mongoose.Schema.Types.ObjectId;
  readonly idTeam: mongoose.Schema.Types.ObjectId;
}
