import * as mongoose from 'mongoose';

export class EditResultDto {
    readonly _id: mongoose.Schema.Types.ObjectId;
    readonly titleResult: string;
    readonly score: number;
    readonly idRace: mongoose.Schema.Types.ObjectId;
    readonly idTeam: mongoose.Schema.Types.ObjectId;
}
