import { Inject, Injectable } from '@nestjs/common';

import { Model } from 'mongoose';
import * as mongoose from 'mongoose';

import { Result } from './intefaces/result.interface';

import { CreateResultDto } from './dto/create-result.dto';
import { EditResultDto } from './dto/edit-result.dto';

@Injectable()
export class ResultsService {
  constructor(
    @Inject('RESULT_MODEL')
    private readonly resultModel: Model<Result>,
  ) {}

  async create(createResultDto: CreateResultDto): Promise<Result> {
    return this.resultModel.create(createResultDto);
  }

  async findAll(): Promise<Result[]> {
    return await this.resultModel.find().exec();
  }

  async editOne(editResultDto: EditResultDto): Promise<Result> {
    return await this.resultModel.updateOne({_id: editResultDto._id}, editResultDto);
  }

  async deleteOne(id: string): Promise<mongoose.mquery> {
    return await this.resultModel.deleteOne({_id: id});
  }
}
