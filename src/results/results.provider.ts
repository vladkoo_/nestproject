import { Connection } from 'mongoose';
import { ResultSchema } from '../database/schemas/result.schema';

export const ResultsProviders = [
    {
        provide: 'RESULT_MODEL',
        useFactory: (connection: Connection) => connection.model('Result', ResultSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
