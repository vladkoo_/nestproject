import { Module } from '@nestjs/common';

import { DatabaseModule } from '../database/database.module';
import { ResultsProviders } from './results.provider';
import { ResultsService } from './results.service';
import { ResultsController } from './results.controller';

@Module(
    {
      imports: [DatabaseModule],
      controllers: [ResultsController],
      providers: [
        ResultsService,
        ...ResultsProviders,
      ],
    },
)
export class ResultsModule {}
