import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface Result extends Document {
  readonly _id: mongoose.Schema.Types.ObjectId;
  readonly titleResult: string;
  readonly score: number;
  readonly idRace: mongoose.Schema.Types.ObjectId;
  readonly idTeam: mongoose.Schema.Types.ObjectId;
}
