import { Module } from '@nestjs/common';

import { DatabaseModule } from '../database/database.module';
import { TeamsProviders } from './teams.provider';
import { TeamsService } from './teams.service';
import { TeamsController } from './teams.controller';

@Module(
    {
      imports: [DatabaseModule],
      controllers: [TeamsController],
      providers: [
        TeamsService,
        ...TeamsProviders,
      ],
    },
)
export class TeamsModule {}
