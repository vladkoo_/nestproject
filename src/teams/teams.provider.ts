import { Connection } from 'mongoose';
import { TeamSchema } from '../database/schemas/team.schema';

export const TeamsProviders = [
    {
        provide: 'TEAM_MODEL',
        useFactory: (connection: Connection) => connection.model('Team', TeamSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
