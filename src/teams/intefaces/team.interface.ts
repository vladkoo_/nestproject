import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface Team extends Document {
  readonly _id: mongoose.Schema.Types.ObjectId;
  readonly titleTeam: string;
  readonly members: number;
  readonly idUser: mongoose.Schema.Types.ObjectId;
}
