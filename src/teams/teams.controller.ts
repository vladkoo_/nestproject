import { Body, Controller, Delete, Get, Param, Post, Put, UploadedFile, UseInterceptors } from '@nestjs/common';

import { CreateTeamDto } from './dto/create-team.dto';
import { EditTeamDto } from './dto/edit-team.dto';

import { Team } from './intefaces/team.interface';

import { TeamsService } from './teams.service';

import { diskStorage } from 'multer';
import { extname } from 'path';
import { FileInterceptor } from '@nestjs/platform-express/multer';

@Controller('teams')
export class TeamsController {
  constructor(private readonly teamsService: TeamsService) {}

  @Get()
  async findAll(): Promise<Team[]> {
    return this.teamsService.findAll();
  }

  @Post()
  async create(@Body() createTeamDto: CreateTeamDto) {
    return this.teamsService.create(createTeamDto);
  }

  @Put()
  async editOne(@Body() editTeamDto: EditTeamDto) {
    return this.teamsService.editOne(editTeamDto);
  }

  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    return this.teamsService.deleteOne(id);
  }

  @UseInterceptors(FileInterceptor('file',
    {
      storage: diskStorage({
        destination: './public',
        filename: (req, file, cb) => {
          const logoId = Array(32).fill(null).map(() => (Math.round(Math.random() *
          16 )).toString(16)).join('');
          return cb(null, `${logoId}${extname(file.originalname)}`, logoId);
        },
      }),
    }))
  @Post(':id/logo')
  async createLogo(@Param('id') id, @UploadedFile() file) {
    return this.teamsService.createLogo(id, `${file.filename}`);
  }
}
