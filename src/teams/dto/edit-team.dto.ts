import * as mongoose from 'mongoose';

export class EditTeamDto {
    readonly _id: mongoose.Schema.Types.ObjectId;
    readonly titleTeam: string;
    readonly members: number;
    readonly idUser: mongoose.Schema.Types.ObjectId;
}
