import * as mongoose from 'mongoose';

export class EditTeamDto {
  readonly idTeam: mongoose.Schema.Types.ObjectId;
  readonly logoUrl: string;
}
