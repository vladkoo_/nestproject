import * as mongoose from 'mongoose';

export class CreateTeamDto {
  readonly titleTeam: string;
  readonly members: number;
  readonly idUser: mongoose.Schema.Types.ObjectId;
}
