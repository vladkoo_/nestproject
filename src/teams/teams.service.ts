import { Inject, Injectable } from '@nestjs/common';

import { Model } from 'mongoose';
import * as mongoose from 'mongoose';

import { Team } from './intefaces/team.interface';

import { CreateTeamDto } from './dto/create-team.dto';
import { EditTeamDto } from './dto/edit-team.dto';
import { EditMongooseDto } from '../database/dto/edit-mongoose.dto';

@Injectable()
export class TeamsService {
  constructor(
    @Inject('TEAM_MODEL')
    private readonly teamModel: Model<Team>,
  ) {}

  async create(createTeamDto: CreateTeamDto): Promise<Team> {
    return this.teamModel.create(createTeamDto);
  }

  async findAll(): Promise<Team[]> {
    return await this.teamModel.find().exec();
  }

  async editOne(editTeamDto: EditTeamDto): Promise<Team> {
    return await this.teamModel.updateOne({_id: editTeamDto._id}, editTeamDto);
  }

  async deleteOne(id: string): Promise<mongoose.mquery> {
    return await this.teamModel.deleteOne({_id: id});
  }

  async createLogo(teamId: string, imageUrl: string): Promise<EditMongooseDto> {
    await this.teamModel.updateOne({_id: teamId}, {teamLogo: imageUrl});
    return await this.teamModel.findOne({_id: teamId});
  }
}
