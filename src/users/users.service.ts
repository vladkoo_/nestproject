import { Inject, Injectable } from '@nestjs/common';

import { Model } from 'mongoose';
import * as mongoose from 'mongoose';

import { User } from './intefaces/user.interface';

import { CreateUserDto } from './dto/create-user.dto';
import { EditUserDto } from './dto/edit-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @Inject('USER_MODEL')
    private readonly userModel: Model<User>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    return this.userModel.create(createUserDto);
  }

  async findAll(): Promise<User[]> {
    return await this.userModel.find().exec();
  }

  async editOne(editUserDto: EditUserDto): Promise<User> {
    return await this.userModel.updateOne({_id: editUserDto._id}, editUserDto);
  }

  async deleteOne(id: string): Promise<mongoose.mquery> {
    return await this.userModel.deleteOne({_id: id});
  }
}
