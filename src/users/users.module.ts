import { Module } from '@nestjs/common';

import { DatabaseModule } from '../database/database.module';
import { UsersProviders } from './users.provider';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';

@Module(
    {
      imports: [DatabaseModule],
      controllers: [UsersController],
      providers: [
        UsersService,
        ...UsersProviders,
      ],
    },
)
export class UsersModule {}
