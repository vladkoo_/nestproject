import * as mongoose from 'mongoose';

export class CreateUserDto {
  readonly firstName: string;
  readonly lastName: string;
  readonly age: number;
  readonly email: string;
}
