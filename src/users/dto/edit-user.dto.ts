import * as mongoose from 'mongoose';

export class EditUserDto {
    readonly _id: mongoose.Schema.Types.ObjectId;
    readonly firstName: string;
    readonly lastName: string;
    readonly age: number;
    readonly email: string;
}
