import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';

import { CreateUserDto } from './dto/create-user.dto';
import { EditUserDto } from './dto/edit-user.dto';

import { User } from './intefaces/user.interface';

import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  async findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Put()
  async editOne(@Body() editUserDto: EditUserDto) {
    return this.usersService.editOne(editUserDto);
  }

  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    return this.usersService.deleteOne(id);
  }
}
