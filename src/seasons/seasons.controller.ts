import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';

import { CreateSeasonDto} from './dto/create-season.dto';
import { EditSeasonDto } from './dto/edit-season.dto';
import { DeleteTreeSeasonDto } from './dto/deleteTree-season.dto';

import { Season } from './intefaces/season.interface';

import { SeasonsService } from './seasons.service';
import { ApiUseTags, ApiOperation, ApiResponse, ApiInternalServerErrorResponse, ApiBadRequestResponse } from '@nestjs/swagger';
import { DeleteMongooseDto } from '../database/dto/delete-mongoose.dto';
import { EditMongooseDto } from '../database/dto/edit-mongoose.dto';

@ApiUseTags('seasons')
@Controller('seasons')
export class SeasonsController {
  constructor(private readonly seasonService: SeasonsService) {}

  @ApiOperation({ title: 'Get all seasons' })
  @ApiResponse({
    status: 200,
    isArray: true,
    type: EditSeasonDto,
    description: 'OK',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @Get()
  async findAll(): Promise<Season[]> {
    return this.seasonService.findAll();
  }

  @ApiOperation({ title: 'Create new season' })
  @ApiResponse({
    status: 200,
    isArray: false,
    type: EditSeasonDto,
    description: 'OK',
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @Post()
  async create(@Body() createSeasonDto: CreateSeasonDto) {
    return this.seasonService.create(createSeasonDto);
  }

  @ApiOperation({ title: 'Edit season' })
  @ApiResponse({
    status: 200,
    isArray: false,
    type: EditMongooseDto,
    description: 'OK',
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @Put()
  async editOne(@Body() editSeasonDto: EditSeasonDto) {
    return this.seasonService.editOne(editSeasonDto);
  }

  @ApiOperation({ title: 'Delete season' })
  @ApiResponse({
    status: 200,
    isArray: false,
    type: DeleteMongooseDto,
    description: 'OK',
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    return this.seasonService.deleteOne(id);
  }

  @ApiOperation({ title: 'Delete all data which connect with season' })
  @ApiResponse({
    status: 200,
    isArray: false,
    type: DeleteTreeSeasonDto,
    description: 'OK',
  })
  @ApiBadRequestResponse({
    description: 'Bad request',
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal Server Error',
  })
  @Delete('deleteTree/:id')
  async cascadeDelete(@Param('id') id: string): Promise<DeleteTreeSeasonDto> {
    return this.seasonService.cascadeDelete(id);
  }
}
