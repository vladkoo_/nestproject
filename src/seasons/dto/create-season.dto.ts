import { ApiModelProperty } from '@nestjs/swagger';

export class CreateSeasonDto {
  @ApiModelProperty({
    example: 'Summer Season',
    type: 'string',
  })
  readonly titleSeason: string;
}
