import * as mongoose from 'mongoose';
import { ApiModelProperty } from '@nestjs/swagger';

export class EditSeasonDto {
    @ApiModelProperty({
        example: '5ca4d7214c86ff1fec4e72ba',
        type: 'string',
    })
    readonly _id: mongoose.Schema.Types.ObjectId;

    @ApiModelProperty({
        example: 'Summer Season',
        type: 'string',
    })
    readonly titleSeason: string;
}
