import * as mongoose from 'mongoose';
import { ApiModelProperty } from '@nestjs/swagger';
import { Season } from '../intefaces/season.interface';
import { League } from '../../leagues/intefaces/league.interface';
import { Stage } from '../../stages/intefaces/stage.interface';
import { Race } from '../../races/intefaces/race.interface';
import { Result } from '../../results/intefaces/result.interface';

export class DeleteTreeSeasonDto {
  @ApiModelProperty({
    example: '5ca4d7214c86ff1fec4e72ba',
    type: 'string',
  })
  readonly _id: mongoose.Schema.Types.ObjectId;

  @ApiModelProperty({
    type: 'array',
  })
  readonly Seasons: Season[];

  @ApiModelProperty({
    type: 'array',
  })
  readonly Leagues: League[];

  @ApiModelProperty({
    type: 'array',
  })
  readonly Stage: Stage[];

  @ApiModelProperty({
    type: 'array',
  })
  readonly Race: Race[];

  @ApiModelProperty({
    type: 'array',
  })
  readonly Result: Result[];
}
