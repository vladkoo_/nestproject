import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface Season extends Document {
  readonly _id: mongoose.Schema.Types.ObjectId;
  readonly titleSeason: string;
}
