import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { SeasonsService } from './seasons.service';
import { SeasonsController } from './seasons.controller';

import { SeasonsProviders} from './seasons.providers';
import { LeaguesProviders } from '../leagues/leagues.provider';
import { StagesProviders } from '../stages/stages.provider';
import { RacesProviders } from '../races/races.provider';
import { ResultsProviders } from '../results/results.provider';

@Module(
  {
    imports: [DatabaseModule],
    controllers: [SeasonsController],
    providers: [
      SeasonsService,
      ...SeasonsProviders,
      ...LeaguesProviders,
      ...StagesProviders,
      ...RacesProviders,
      ...ResultsProviders,
    ],
  })
export class SeasonsModule {}
