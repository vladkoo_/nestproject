import { Connection } from 'mongoose';
import { SeasonSchema } from '../database/schemas/season.schema';

export const SeasonsProviders = [
  {
    provide: 'SEASON_MODEL',
    useFactory: (connection: Connection) => connection.model('Season', SeasonSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
