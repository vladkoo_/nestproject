import { Model } from 'mongoose';
import * as mongoose from 'mongoose';

import { Injectable, Inject } from '@nestjs/common';

import { Season } from './intefaces/season.interface';
import { League } from '../leagues/intefaces/league.interface';
import { Stage } from '../stages/intefaces/stage.interface';
import { Race } from '../races/intefaces/race.interface';
import { Result } from '../results/intefaces/result.interface';

import { CreateSeasonDto } from './dto/create-season.dto';
import { EditSeasonDto } from './dto/edit-season.dto';
import { DeleteTreeSeasonDto } from './dto/deleteTree-season.dto';

@Injectable()
export class SeasonsService {
  constructor(
    @Inject('SEASON_MODEL') private readonly seasonModel: Model<Season>,
    @Inject('LEAGUE_MODEL') private readonly leagueModel: Model<League>,
    @Inject('STAGE_MODEL') private readonly stageModel: Model<Stage>,
    @Inject('RACE_MODEL') private readonly raceModel: Model<Race>,
    @Inject('RESULT_MODEL') private readonly resultModel: Model<Result>,
  ) {}

  async create(createSeasonDto: CreateSeasonDto): Promise<Season> {
    return this.seasonModel.create(createSeasonDto);
  }

  async findAll(): Promise<Season[]> {
    return await this.seasonModel.find().exec();
  }

  async editOne(editSeasonDto: EditSeasonDto): Promise<Season> {
    return await this.seasonModel.updateOne({_id: editSeasonDto._id}, editSeasonDto);
  }

  async deleteOne(id: string): Promise<mongoose.mquery> {
    return await  this.seasonModel.deleteOne({_id: id});
  }

  async cascadeDelete(id: string): Promise<DeleteTreeSeasonDto> {
    const data = await this.seasonModel.aggregate([
      {
        $match: { _id: { $eq: mongoose.Types.ObjectId(id) } },
      },
      {
        $lookup: {
          localField: '_id',
          from: 'leagues',
          foreignField: 'idSeason',
          as: 'Leagues',
        },
      },
      {
        $unwind: {
          path: '$Leagues',
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          localField: 'Leagues._id',
          from: 'stages',
          foreignField: 'idLeague',
          as: 'Stages',
        },
      },
      {
        $unwind: {
          path: '$Stages',
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          localField: 'Stages._id',
          from: 'races',
          foreignField: 'idStage',
          as: 'Races',
        },
      },
      {
        $unwind: {
          path: '$Races',
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $lookup: {
          localField: 'Races._id',
          from: 'results',
          foreignField: 'idRace',
          as: 'Results',
        },
      },
      {
        $unwind: {
          path: '$Results',
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $group: {
          _id: '$_id',
          Seasons: {
            $addToSet: '$_id',
          },
          Leagues: {
            $addToSet: '$Leagues._id',
          },
          Stages: {
            $addToSet: '$Stages._id',
          },
          Races: {
            $addToSet: '$Races._id',
          },
          Results: {
            $addToSet: '$Results._id',
          },
        },
      },
    ]);
    await this.seasonModel.remove({ _id: {$in: data[0].Seasons} } );
    await this.leagueModel.remove({_id: {$in: data[0].Leagues}});
    await this.stageModel.remove({_id: {$in: data[0].Stages}});
    await this.raceModel.remove({_id: {$in: data[0].Races}});
    await this.resultModel.remove({_id: {$in: data[0].Results}});
    return data;
  }
}
