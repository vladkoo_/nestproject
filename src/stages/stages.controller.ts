import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';

import { CreateStageDto } from './dto/create-stage.dto';
import { EditStageDto } from './dto/edit-stage.dto';

import { Stage } from './intefaces/stage.interface';

import { StagesService } from './stages.service';

@Controller('stages')
export class StagesController {
  constructor(private readonly stagesService: StagesService) {}

  @Get()
  async findAll(): Promise<Stage[]> {
    return this.stagesService.findAll();
  }

  @Post()
  async create(@Body() createStageDto: CreateStageDto) {
    return this.stagesService.create(createStageDto);
  }

  @Put()
  async editOne(@Body() editStageDto: EditStageDto) {
    return this.stagesService.editOne(editStageDto);
  }

  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    return this.stagesService.deleteOne(id);
  }
}
