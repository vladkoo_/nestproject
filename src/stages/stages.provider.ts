import { Connection } from 'mongoose';
import { StageSchema } from '../database/schemas/stage.schema';

export const StagesProviders = [
    {
        provide: 'STAGE_MODEL',
        useFactory: (connection: Connection) => connection.model('Stage', StageSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
