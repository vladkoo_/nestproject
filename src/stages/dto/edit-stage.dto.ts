import * as mongoose from 'mongoose';

export class EditStageDto {
    readonly _id: mongoose.Schema.Types.ObjectId;
    readonly titleStage: string;
}
