import { Module } from '@nestjs/common';

import { DatabaseModule } from '../database/database.module';
import { StagesProviders } from './stages.provider';
import { StagesService } from './stages.service';
import { StagesController } from './stages.controller';

@Module(
    {
      imports: [DatabaseModule],
      controllers: [StagesController],
      providers: [
        StagesService,
        ...StagesProviders,
      ],
    },
)
export class StagesModule {}
