import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface Stage extends Document {
  readonly _id: mongoose.Schema.Types.ObjectId;
  readonly titleStage: string;
}
