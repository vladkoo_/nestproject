import { Inject, Injectable } from '@nestjs/common';

import { Model } from 'mongoose';
import * as mongoose from 'mongoose';

import { Stage } from './intefaces/stage.interface';

import { CreateStageDto } from './dto/create-stage.dto';
import { EditStageDto } from './dto/edit-stage.dto';

@Injectable()
export class StagesService {
  constructor(
    @Inject('STAGE_MODEL')
    private readonly stageModel: Model<Stage>,
  ) {}

  async create(createStageDto: CreateStageDto): Promise<Stage> {
    return this.stageModel.create(createStageDto);
  }

  async findAll(): Promise<Stage[]> {
    return await this.stageModel.find().exec();
  }

  async editOne(editStageDto: EditStageDto): Promise<Stage> {
    return await this.stageModel.updateOne({_id: editStageDto._id}, editStageDto);
  }

  async deleteOne(id: string): Promise<mongoose.mquery> {
    return await this.stageModel.deleteOne({_id: id});
  }
}
